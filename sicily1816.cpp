//题目分析:
//个人觉得class还是多写比较好，不要嫌烦，
//虽然的确很烦，但毕竟是C++精华所在。
//所以练练手吧。没什么好分析的。

//题目网址:http://soj.me/1816

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

const double PI = acos(-1.0);
const double SQR2 = sqrt(2.0);
const double SQR3 = sqrt(3.0);

class Geometry {
    public:
        virtual double getPara() = 0;
        virtual double getArea() = 0;
        virtual double getCirc() = 0;
        virtual double getDiag() = 0;
};

class Circle: public Geometry {
    public:
        Circle(double r = 0.0) { this->r = r; }
        double getPara() { return this->r; }
        double getArea() { return this->r * this->r * PI; }
        double getCirc() { return 2.0 * PI * this->r; }
        double getDiag() { return 2.0 * this->r; }
    private:
        double r;
};

class Rectangle: public Geometry {
    public:
        Rectangle(double e = 0.0) { this->e = e; }
        double getPara() { return this->e; }
        double getArea() { return this->e * this->e; }
        double getCirc() { return 4.0 * this->e; }
        double getDiag() { return sqrt(2.0 * this->e * this->e); }
    private:
        double e;
};

class Triangle: public Geometry {
    public:
        Triangle(double b = 0.0) { this->b = b; }
        double getPara() { return this->b; }
        double getArea() { return SQR3 / 4.0 * this->b * this->b; }
        double getCirc() { return 3.0 * this->b; }
        double getDiag() { return SQR3 / 2.0 * this->b; }
    private:
        double b;
};

int main()
{
    int testCases;
    double r;
    cin >> testCases;
    while (testCases--) {
        cin >> r;

        Circle c1(r);
        cout << setiosflags(ios::fixed)
             << setprecision(4) << c1.getArea() << " " 
             << setprecision(4) << c1.getCirc() << endl;

        Rectangle s1(2*r);
        cout << setiosflags(ios::fixed)
             << setprecision(4) << s1.getArea() << " " 
             << setprecision(4) << s1.getCirc() << " " 
             << setprecision(4) << s1.getDiag() << endl;

        Circle c2(s1.getDiag() / 2);
        cout << setiosflags(ios::fixed)
             << setprecision(4) << c2.getArea() << " " 
             << setprecision(4) << c2.getCirc() << endl;

        Triangle t(c2.getPara() * 2.0 * SQR3);
        cout << setiosflags(ios::fixed)
             << setprecision(4) << t.getArea() << " " 
             << setprecision(4) << t.getCirc() << endl;

    }
    return 0;
}